var token = "";
function initWs(store, token) {
	var socketTask = uni.connectSocket({
		url: "ws://120.48.114.114:44584/ws",
		success: function (res) {
			console.log("connect success");
		}
	});
	socketTask.onOpen((res) => {
		socketTask.send({
			data: token,
			success: function (res) {
				console.log("login success");
			}
		})
	});
	socketTask.onClose((res) => {
		store.commit("clearWebsocketConnection");
	});
	socketTask.onError((res) => {
		store.commit("clearWebsocketConnection");
	});
	store.commit("setWebsocketConnection", socketTask);
}
export default{
	token,
	initWs
}