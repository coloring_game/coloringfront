import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
Vue.config.productionTip = false
App.mpType = 'app'
import $global from './common/global.js';
Vue.prototype.$global = $global;
// Vue.prototype.$url = "http://8.142.10.90:44583/";
// Vue.prototype.$url = "http://localhost:44584/"; 
Vue.prototype.$url = "http://120.48.114.114:44584/"; 
const store = new Vuex.Store({
  state: {
    websocketConnection : null
  },
  mutations: {
    setWebsocketConnection(state, websocketConnection){
      state.websocketConnection = websocketConnection;
    },
    clearWebsocketConnection(state){
      state.websocketConnection = null;
    },
    heartBeat(state, token){
	  console.log(token);
      state.websocketConnection.send({
        data: token,
        success: function (res) {
          console.log("heartBeat success");
        }
      })
    }
  }
})
Vue.prototype.$store = store
const app = new Vue({
    ...App,
    store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif